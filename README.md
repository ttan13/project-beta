# CarCar

Team:

* Nick Wagemann - Sales
* Teresa Tan - Service
<p>&nbsp;</p>


## Initialization
Run the following commands in your terminal after you have cloned the project from the main branch

1) ```docker volume create beta-data``` (Builds a new volume that the containers can consume and store data in)

2) ```docker compose build``` (Builds images as per the docker-compose.yml file)

3) ```docker compose up``` (Starts or restarts all services defined in the docker-compose.yml)

4) Open browser to http://localhost:3000 to view the home page.
<p>&nbsp;</p>


## Design
We started by working together in [Excalidraw](https://excalidraw.com/#room=0f44eaab5c08dac4624c,2vOC4MVxqogn7K-tuU1SWg) by planning our site layout and databases with a domain-driven design. This helped give a good reference throughout the project for us to view the organizational relationships with each other as well as the site design.
<p>&nbsp;</p>


## Service Microservice - Teresa Tan

3 models were created: 
- Service Appointment
- Technician
- Automobile Value Object 
<p>&nbsp;</p>

The technician and service appointment models can create its own respective objects. The service appointment model has a foreign key to the technician model in order to create an appointment.

The Automobile VO polls data from the Automobile model within the Inventory micro service, which allowed access to the unique vin number to use within the Service micro service. 

<u>Technician Views</u>
- Create
- List

<u>Appointment Views</u>
- Create
- List
- Update
- Delete

<u>Service history View</u>
- List
<p>&nbsp;</p>



## Sales Microservice - Nick Wagemann 

The Sales Microservice has four models.

One the automobile value object that polls data from Automobile in the inventory models and lets me gain access to the vin number from inventory to use in the sales microservice.
 
The employee model is used to create an employee and the customer model is used to create a customer. 

The Record Model is reliant upon the other three models to generate records that will have each of them listed within.

I created views that give the ability to get list, view details of specific items, update particular items, delete specific items, and post new items for the employee, customer, and record model.

These views helped provide the information to fetch necessary information for the react microservice on the front end.

You can use all that functionality in Insomnia and some of the necessary lists and view features on the browser.

Insomnia requests:
For Sales Microservice 

Records
List Records: http://localhost:8090/api/record/
Detail Records: http://localhost:8090/api/record/(id number)/
Update Records: http://localhost:8090/api/record/(id number)/
Delete Records: http://localhost:8090/api/record/(id number)/
Create Records: http://localhost:8090/api/record/
for create need to use this format for json Data
example:
{
	"price": 115000.99,
	"employee_id": 1,
	"customer_id": 4,
	"automobile": 5
}

Employee
List Employee: http://localhost:8090/api/customer/
Detail Employee: http://localhost:8090/api/employee/(id number)/
Update Employee: http://localhost:8090/api/employee/(id number)/
Delete Employee: http://localhost:8090/api/employee/(id number)/
Create Employee: http://localhost:8090/api/employee/
for create need to use this format for json Data
example:
{
	"employee_name": "Robert Wagemann",
	"employee_id": 2
}

Customer 
List Employee: http://localhost:8090/api/customer/
Detail Employee: http://localhost:8090/api/customer/(id number)/
Update Employee: http://localhost:8090/api/customer/(id number)/
Delete Employee: http://localhost:8090/api/customer/(id number)/
Create Employee: http://localhost:8090/api/customer/
for create need to use this format for json Data
example:
{
	"customer_name": "Ace Wagemann",
	"address": "1011 4th street West Babylon, Ny 11704",
	"phone_number": "16318840777"
}



